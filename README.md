[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 2

Problemas propuestos para la Sesión 2 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

Los ejercicios son diferentes para cada grupo:

- [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion2#grupo-1)
- [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion2#grupo-2)
- [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion2#grupo-3)
- [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion2#grupo-4)

### Grupo 1

En el ejercicio se pretende simular la realización de un pedido de ordenadores por parte de unos proveedores a petición del hilo principal. Esperará por un tiempo que se complete el pedido y se pedirá su cancelación. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Fabricante`: Clase que representa a un fabricante para un `TipoComponente` de un `Ordenador`.
- `Ordenador`: Tiene un componente de cada uno de los presentes en `TipoComponente` realizados por un `Fabricante`.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Proveedor`: Representa a un proveedor que debe realizar un pedido de ordenadores que le solicite el hilo principal. Tiene las siguientes variables de instancia:
	- Un identificador.
	- Una cantidad de ordenadores que tendrá el pedido.
	- Una lista de ordenadores que componen el pedido.
	- Los pasos que debe realizar son:
		- Preparar el pedido: 
			- El pedido podrá interrumpirse, cancelarse, si no se ha completado ya la mitad de las unidades que lo componen.
			- Crea un ordenador compuesto por un componente realizado por un fabricante de todos los presentes en `TipoComponente`.
			- Se simula un tiempo para completar un ordenador que estará comprendido entre 5 y 7 segundos.
			- A la finalización del pedido, o interrupción, se debe presentar el pedido realizado. Debe quedar claro si el pedido ha sido cancelado.

- `Hilo principal`: Realizará los siguientes pasos:
	- Crear 10 proveedores 
		- Cada proveedor tiene que realizar un pedido de entre 1 a 10 ordenadores.
		- Asociar un objeto `Thread` a cada proveedor para su ejecución.
	- Ejecutar los hilos.
	- Se suspende por 30 segundos para que se completen los pedidos.
	- Pasado este tiempo solicita la interrupción de los pedidos que no se hayan completado en ese tiempo.
	- Espera a que todos los hilos finalicen su ejecución antes de finalizar.


### Grupo 2

Una granja de rendering (render farm) tiene Asignadores de Fotogramas a ordenadores para asignar a cada ordenador los fotogramas que tiene que calcular. Cada `Asignador` tiene varios ordenadores a su cargo (que son distintos a los ordenadores de los otros Asignadores) y varios fotogramas que asignar (que son distintos a los fotogramas de los demas asignadores). Su misión es asignar sus fotogramas a sus ordenadores siempre que sean compatibles.

Un `Ordenador` tiene unas capacidades gráficas que pueden ser **básicas**, **medias** o **avanzadas**. Un `Fotograma` tiene unas necesidades de capacidad gráfica que pueden ser **básicas**, **medias** o **avanzadas**. Un `Fotograma` y un `Ordenador` son compatibles si coinciden su necesidad gráfica con su capacidad gráfica.

Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Fotograma`: Simula un fotograma que asignar. Tiene una necesidad gráfica concreta y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Ordenador`: Simula un ordenador al que se le asignan fotogramas.

El ejercicio consiste en completar la implementación de las siguientes clases (cuyo esqueleto se incluye en el material de prácticas):

- `Asignador`: Es quien realiza la tarea de asignar fotogramas a ordenadores. Tiene que tener las siguientes variables de instancia:
	- Un identificador.
	- Un array con los fotogramas que tiene que asignar.
	- Un array de ordenadores a los que tiene que asignar fotogramas.
- El `Asignador` realizará las siguientes tareas:
	- A su constructor se le pasarán dos arrays, uno de fotogramas y otro de ordenadores generados aleatoriamente.
	- Para cada `Fotograma`:
		- Asignarlo a un `Ordenador` compatible.
		- Esperar el tiempo de asignación que devuelve el propio `Fotograma`.
	- En cualquier momento se podría solicitar la cancelación de la tarea, sin embargo el `Asignador` no debe cancelarse si no se han asignado al menos el 80% de los fotogramas.
	- Al finalizar la tarea el `Asignador` debe mostrar el estado de los ordenadores (incluyendo los fotogramas que cada uno tiene asignado) y el número de fotogramas que hayan quedado sin asignar porque se haya cancelado la tarea antes de terminar.

- `Hilo principal`: Realizará las siguientes tareas:
	- Crear 10 asignadores y para cada uno de ellos:
		- Crear un array de entre 3 y 5 ordenadores.
		- Crear un array de entre 5 y 7 fotogramas.
	- Asociar un hilo distinto a cada `Asignador`
	- Ejecutar los hilos.
	- Esperar 20 segundos y transcurrido ese tiempo solicitar la cancelación de los hilos que no hayan terminado.
	- Esperar a que finalicen todos los hilos antes de que el hilo principal finalice.

### Grupo 3

Este ejercicio pretende simular la ejecución de una lista de procesos en una unidad de procesamientos creadas en el hilo principal. Se esperará por un tiempo para que se complete la ejecución de los procesos y se pedirá la cancelación de los que no se han completado. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Proceso`: Representa al proceso que simulará su ejecución en una unidad de procesamiento.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `UnidadProcesamiento`: Simula la unidad donde se ejecutarán una lista de procesos. Tiene que tener las siguientes variables de instancia:
	- Un identificador.
	- Una lista de procesos a ejecutar.
	- Las acciones que tiene que realizar son las siguientes:
		- Recorrer la lista de procesos a ejecutar y para cada proceso:
			- Puede ser solicitada la interrupción y no será atendida si ya se ha completado el 80% de los procesos asignados. Debe quedar claro si se ha interrumpido la ejecución de los procesos.
			- Simular la ejecución de un proceso, tiene un método que proporciona el tiempo de ejecución que tiene. Utilizar adecuadamente el enumerado `EstadoEjecucion` para simular el estado en que se encuentra un proceso.
		- Presentar la lista de procesos con el estado final que han alcanzado, es decir, si se ha atendido la interrupción no todos los procesos pueden mostrar el estado de `FINALIZACION`.

- `Hilo principal`: Realizará las siguientes tareas:
	- Crear 10 unidades de procesamiento y para cada una de ellas:
		- Crear una lista de procesos aleatorios de entre 7 y 10.
		- Asociar un objeto `Thread` a cada unidad de procesamiento.
	- Ejecutar los hilos
	- Esperar por 30 segundos para la ejecución de los procesos.
	- Pasado ese tiempo solicitar la interrupción de las unidades de procesamiento.
	- Esperar a que finalicen todos los hilos antes de que el hilo principal finalice.

### Grupo 4

En el ejercicio se intenta simular la instalación de sensores en las habitaciones de una lista de casas que tiene un instalador. La petición de instalación podrá cancelarse si no se han completado en un tiempo establecido. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Sensor`: Simula un tipo de sensor y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Casa`: Simula donde se instalará un sensor. Dispone de una distribución de habitaciones según el `TipoCasa` que viene definido en `Constantes`.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Instalador`: Simula la persona encargada de la instalación de una lista de sensores en una lista de casas. Las variables de instancia son:
	- Un identificador.
	- Una lista de sendores a instalar.
	- La lista de casas donde se podrá instalar la lista de sensores.
	- Las tareas que debe completar son:
		- Tomar un sensor de la lista para instalarlo en una casa:
			- Se puede solicitar la interrupción de la instalación de los sensores siempre que no se haya ya completado el 80% de la lista de sensores. Tiene que quedar claro si se ha interrumpido la instalación de los sensores.
			- En contrar una casa donde instalar el sensor.
			- Si se encuentra una casa donde instalarlo se simulará el tiempo de instalación que tiene asociado ese sensor.
		- Cuando se complete el trabajo del instalador se debe presentar el estado en que han quedado las casas antes de finalizar. También se debe mostrar los sensores no instalados, o porque se ha interrumpido la instalación o porque no había casas donde instalarlos.

- `Hilo principal`: Realiza las siguientes tareas:
	- Crea 10 instaladores y para cada uno de ellos:
		- Crea una lista de sensores a instalar con un número de entre 5 y 7 generados aleatoriamente.
		- Crea una lista de casas donde instalar los sensores de entre 3 y 5. El tipo de casa es aleatorio de entre `TipoCasa`.
		- A cada instalador se le asocia un hilo para su ejecución.
	- Ejecuta los hilos de los instaladores.
	- Espera por 20 segundos ha que terminen los instaladores.
	- Una vez pasado el tiempo solicita la cancelación de los instaladores que no hayan terminado.
	- Espera ha que finalicen todos los hilos antes de que el hilo principal termine su ejecución.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTkwMjMyNDUyLDE5MjA5NjU3MjUsMTY5Nj
QwMzc3OF19
-->