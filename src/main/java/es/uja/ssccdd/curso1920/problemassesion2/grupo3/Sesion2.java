/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.EstadoEjecucion.LISTO;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.NUM_PROCESOS;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.TOTAL_NUCLEOS;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.aleatorio;
import es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.TipoProceso;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        Thread[] listaHilos;
        
        
        // Inicialización de las variables para la prueba
        listaHilos = new Thread[TOTAL_NUCLEOS];
        
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Creación de las unidades de procesamiento con su lista de procesos
        // para ejecutar y asignarle su hilo de ejecución
        for( int i = 0; i < TOTAL_NUCLEOS; i++ ) {
            // Lista de procesos para ejecutar en cada unidad de procesamiento
            ArrayList<Proceso> listaProcesos = new ArrayList();
            int numProcesos = aleatorio.nextInt(VARIACION) + NUM_PROCESOS;
            for( int j = 0; j < numProcesos; j++ ) {
                TipoProceso tipoProceso = TipoProceso.getProceso();
                Proceso proceso = new Proceso((i+1)*(j+1),tipoProceso);
                proceso.setEstado(LISTO);
                listaProcesos.add(proceso);
            }
            
            UnidadProcesamiento nucleo = new UnidadProcesamiento("Nucleo("+i+")",listaProcesos);
            listaHilos[i] = new Thread(nucleo, nucleo.getiD());
        }
        
        // Ejecución de los procesos en cada unidad de procesamiento
        for( Thread hilo : listaHilos ) 
            hilo.start();
        
        // Espera por el tiemo establecido para que se completen las ejecuciones
        // de los procesos
        System.out.println("HILO-Principal Espera a la finalización de los procesos");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación de las unidades de procesamiento que han excedido el tiempo
        System.out.println("HILO-Principal Solicita la finalización de los pedidos");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        for( Thread hilo : listaHilos )
            hilo.join();
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
