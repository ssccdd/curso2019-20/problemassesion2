/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.NUM_CASAS;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.NUM_SENSORES;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.TOTAL_INSTALADORES;
import es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.TipoCasa;
import es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.TipoSensor;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Variables aplicación
        Thread[] listaHilos;
        
        
        // Inicialización de las variables para la prueba
        listaHilos = new Thread[TOTAL_INSTALADORES];
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        
        // Crear la lista de sensores y casas para cada instalador
        for( int i = 0; i < TOTAL_INSTALADORES; i++ ) {
            // Lista de sensores
            int numSensores = aleatorio.nextInt(VARIACION) + NUM_SENSORES;
            ArrayList<Sensor> listaSensores = new ArrayList();
            for(int j = 0; j < numSensores; j++ )
                listaSensores.add(new Sensor((i+1)*(j+1), TipoSensor.getSensor()));
            
            // Lista de casas
            int numCasas = aleatorio.nextInt(VARIACION) + NUM_CASAS;
            ArrayList<Casa> listaCasas = new ArrayList();
            for(int j = 0; j < numCasas; j++ )
                listaCasas.add(new Casa((i+1)*(j+1), TipoCasa.getCasa()));
            
            Instalador instalador = new Instalador("Instalador("+i+")", listaCasas, listaSensores);
            listaHilos[i] = new Thread(instalador, instalador.getiD());
        }
        
        // Instalación de los sensores por los instaladores
        for( Thread hilo : listaHilos ) 
            hilo.start();
        
        // Espera por el tiemo establecido para que se completen los trabajos
        // de instalación de los sensores
        System.out.println("HILO-Principal Espera a la finalización de los procesos");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);
        
        // Se solicita la cancelación del trabajo de los instaladores 
        // que han excedido el tiempo
        System.out.println("HILO-Principal Solicita la finalización de los pedidos");
        for( Thread hilo : listaHilos ) 
            hilo.interrupt();
        
        // Espera a la finalización de todos los hilos
        for( Thread hilo : listaHilos )
            hilo.join();
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
