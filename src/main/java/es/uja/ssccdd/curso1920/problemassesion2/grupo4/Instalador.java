/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo4;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Instalador implements Runnable {
    private final String iD;
    private final ArrayList<Casa> listaCasas;
    private final ArrayList<Sensor> listaSensores;
    private final int numSensores;
    private int sensoresInstalados;
    
    // Constantes
    private final int NO_CANCEL = 80; // % cancelación
    private final int TOTAL = 100; // 100%

    public Instalador(String iD, ArrayList<Casa> listaCasas, ArrayList<Sensor> listaSensores) {
        this.iD = iD;
        this.listaCasas = listaCasas;
        this.listaSensores = listaSensores;
        this.numSensores = listaSensores.size();
        this.sensoresInstalados = 0;
    }

    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza la ejecución de sus " + listaSensores.size() +
                    " sensores asignados en sus " + listaCasas.size() +
                    " casas asignadas");
        
        try {
            for( Sensor sensor : listaSensores )
                instalarSensor(sensor);
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } 
        
        presentarTrabajo();
    }

    public String getiD() {
        return iD;
    }
    
    private void instalarSensor(Sensor sensor) throws InterruptedException {
        boolean sensorAsignado = false;
        int i = 0;
        
        // Asignamos un sensor a una casa si es posible
        while( (i < listaCasas.size()) && !sensorAsignado ) {
            // Comprueba la solicitud de interrupción de la tarea y si no
            // se ha completado el % previsto de sensores
            if ( Thread.interrupted() )
                if ( sensoresInstalados < (numSensores * NO_CANCEL /  TOTAL) )
                    throw new InterruptedException();
                else 
                    System.out.println("HILO-" + Thread.currentThread().getName() 
                        + " La ejecución no puede ser cancelada");
            
            sensorAsignado = listaCasas.get(i).addSensor(sensor);
            i++;
        }
        
        if( sensorAsignado )
            // Simulamos el tiempo de instalación
            try {
                TimeUnit.SECONDS.sleep(sensor.getTiempoInstalacion());
                sensoresInstalados++;
            } catch (InterruptedException ex) {
                // Si se produce la interrupción mientas está suspendido hay que tratarla
                // por si ya se ha superado el % de cancelación y no es posible su finalización
                if ( sensoresInstalados < (numSensores * NO_CANCEL /  TOTAL) )
                    throw new InterruptedException();
                else 
                    System.out.println("HILO-" + Thread.currentThread().getName() 
                        + " La ejecución no puede ser cancelada");
            }   
    }
    
    private void presentarTrabajo() {
        String trabajo = "HILO-" + Thread.currentThread().getName() + 
                         " Sensores intalados en las casas: \n\t";
                
        
        for( Casa casa : listaCasas )
            trabajo = trabajo + casa + "\n\t";
        
        if( sensoresInstalados < numSensores )
            trabajo = trabajo + "Sensores no instalados "
                    + (numSensores - sensoresInstalados);
        
        System.out.println(trabajo);
    }
}
