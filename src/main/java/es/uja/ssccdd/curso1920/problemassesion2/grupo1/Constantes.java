/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo1;

import java.util.Random;

/**
 * Interface con las constantes necesarias para la resolución del ejercico
 * @author pedroj
 */
public interface Constantes {
    // Generador aleatorio
    public static final Random aleatorio = new Random();
    
    // Enumerado para el tipo de componente
    public enum TipoComponente {
        CPU(25), MEMORIA(50), PERIFERICO(100);
        
        private final int valor;

        private TipoComponente(int valor) {
            this.valor = valor;
        }
        
        /**
         * Genera aleatoriamente un TipoComponente 
         * @return
         *      componente generado aleatoriamente
         */
        public static TipoComponente getComponente() {
            int valor = aleatorio.nextInt(D100);
            TipoComponente resultado = null;
            int i = 0;
            
            while( (i < COMPONENTES.length) && (resultado == null) ) {
                if ( COMPONENTES[i].valor > valor )
                    resultado = COMPONENTES[i];
                
                i++;
            }
            
            return resultado;
        } 
    }
    
    // Constantes del problema
    public static final int UNIDADES_MAXIMAS = 20; 
    public static final int TOTAL_PROVEEDORES = 10;
    public static final int D100 = 100; // Simula un dado de 100 caras
    public static final TipoComponente[] COMPONENTES = TipoComponente.values() ;
    public static final int VARIACION = 3;
    public static final int TIEMPO_PREPARACION = 5; // segundos
    public static final int TIEMPO_ESPERA = 30; // segundos
}
