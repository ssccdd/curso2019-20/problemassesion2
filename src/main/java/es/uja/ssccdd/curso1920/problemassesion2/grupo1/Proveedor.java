/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo1.Constantes.COMPONENTES;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo1.Constantes.TIEMPO_PREPARACION;
import es.uja.ssccdd.curso1920.problemassesion2.grupo1.Constantes.TipoComponente;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo1.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final String iD;
    private final int unidadesPedido;
    private final ArrayList<Ordenador> pedido;

    public Proveedor(String iD, int elmementosPedido) {
        this.iD = iD;
        this.unidadesPedido = elmementosPedido;
        this.pedido = new ArrayList();
    }

    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza ha preparar su pedido de " + unidadesPedido +
                    " ordenadores");
        
        try {
            prepararPedido();
        } catch (InterruptedException ex) {
            System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
        } 
        
        
        presentarPedido();
        
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Prepara el pedido de ordenadores solicitado simulando el tiempo de
     * preparación para cada ordenador
     * @throws InterruptedException 
     */
    private void prepararPedido() throws InterruptedException {
        int finalizarPedido = unidadesPedido / 2;
        
        // Se prepara el pedido con los ordenadores solicitados
        for( int i = 0; i < unidadesPedido; i++ ) {
            // Comprueba la solicitud de interrupción de la tarea y si se
            // puede atender.
            if ( Thread.interrupted() )
                if ( pedido.size() < finalizarPedido )
                    throw new InterruptedException();
                else 
                    System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " El pedido no puede ser cancelado");
            
            // Prepara un ordenador con todos sus componentes
            Ordenador ordenador = new Ordenador();
            ordenador.setiD(i);
            for( TipoComponente componente : COMPONENTES ) {
                Fabricante fabricante = new Fabricante(componente.ordinal(),componente);
                ordenador.addComponente(fabricante);
            }
            pedido.add(ordenador);
            
            // Simulamos el tiempo de preparación de un ordenador del pedido
            try {
                int tiempo = aleatorio.nextInt(VARIACION) + TIEMPO_PREPARACION;
                TimeUnit.SECONDS.sleep(tiempo);
            } catch (InterruptedException ex) {
                // Si se produce la interrupción mientas está suspendido hay que tratarla
                // por si ya se ha superado la mitad del trabajo y no es posible su finalización
                if ( pedido.size() < finalizarPedido )
                    throw new InterruptedException();
                else 
                    System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " El pedido no puede ser cancelado");
            }
            
        }
    }
    
    /**
     * Presenta el pedido de ordenadores o un mensaje indicando que se ha
     * cancelado su pedido
     */
    private void presentarPedido() {
        if( pedido.size() == unidadesPedido )
            System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " Pedido solicitado: \n\t" + pedido);
        else
            System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " Han cancelado su pedido");
    }
    
}
