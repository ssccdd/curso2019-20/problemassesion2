/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.MIN_FOTOGRAMAS;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.MIN_ORDENADORES;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.NUM_ASIGNADORES;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.TIEMPO_ESPERA;
import es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.TipoCapacidadGrafica;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.aleatorio;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author fconde
 */
public class Sesion2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // - Construir la lista de hilos que contendran los asignadores --------
        Thread[] hilos;    
        hilos = new Thread[NUM_ASIGNADORES];
        
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // - Crear la lista de Ordenadores y Fotogramas para cada Asignador,
        //   crear el asignador y pasarlo a un hilo
        
        for (int i=0; i<NUM_ASIGNADORES; i++) {
            // - Array ordenadores para el Asignador ---------------------------
            int numOrdenadores = MIN_ORDENADORES + aleatorio.nextInt(VARIACION);
            Ordenador[] ordenadores;
            ordenadores = new Ordenador[numOrdenadores];
            for (int j=0; j<numOrdenadores; j++) {
                ordenadores[j] = new Ordenador(TipoCapacidadGrafica.getCapacidad());
            }
            
            // - Array fotogramas para el Asignador ----------------------------
            int numFotogramas = MIN_FOTOGRAMAS + aleatorio.nextInt(VARIACION);
            Fotograma[] fotogramas;
            fotogramas = new Fotograma[numFotogramas];
            for (int j=0; j<numFotogramas; j++) {
                fotogramas[j] = new Fotograma(j, TipoCapacidadGrafica.getCapacidad());
             }
            
            // - Crear el Asignador --------------------------------------------
            Asignador asignador = new Asignador("Asignador("+i+")", ordenadores, fotogramas);
            // - Crear un hilo con ese Asignador -------------------------------
            hilos[i] = new Thread(asignador, asignador.getID());
        }
        
        // - Iniciar los hilos
        for (Thread h: hilos) {
            h.start();
        }
        
        // - Esperar 20 segundos antes de solicitar la interrupcion de los hilos
        System.out.println("Hilo(PRINCIPAL) Esperar 20 segundos\n");
        TimeUnit.SECONDS.sleep(TIEMPO_ESPERA);

        // - Solicitar la cancelacion de los hilos
        System.out.println("Hilo(PRINCIPAL) Solicitar interrupcion hilos\n");
        for(Thread hilo: hilos) {
            hilo.interrupt();
        }

        System.out.println("Hilo(PRINCIPAL) Esperando la finalizacion de los hilos\n");
        // - Esperar a la cancelacion de todos los hilos
        for(Thread hilo: hilos ) {
            hilo.join();
        }

        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
} 
