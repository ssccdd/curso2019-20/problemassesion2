/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.EstadoEjecucion.CANCELADO;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.EstadoEjecucion.EN_EJECUCION;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo3.Constantes.EstadoEjecucion.FINALIZADO;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class UnidadProcesamiento implements Runnable {
    private final String iD;
    private final ArrayList<Proceso> listaProcesos;
    private int completados;
    
    // Constantes
    private final int NO_CANCEL = 80; // % cancelación
    private final int TOTAL = 100; // 100%

    public UnidadProcesamiento(String iD, ArrayList<Proceso> listaProcesos) {
        this.iD = iD;
        this.listaProcesos = listaProcesos;
        this.completados = 0;
    }
    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza la ejecución de sus " + listaProcesos.size() +
                    " procesos asignados");
        
        try {
            // Se ejecuta la lista de procesos
            for( Proceso proceso : listaProcesos )
                ejecutarProceso(proceso);
            
        } catch (InterruptedException ex) {
            
            cancelacionEjecucion();
        } 
        
        presentarEjecucion();
    }

    public String getiD() {
        return iD;
    }
    
    private void ejecutarProceso(Proceso proceso) throws InterruptedException {
        // Comprueba la solicitud de interrupción de la tarea y si no
        // se ha completado el % previsto de procesos
        if ( Thread.interrupted() )
            if ( completados < (listaProcesos.size() * NO_CANCEL /  TOTAL))
                throw new InterruptedException();
            else 
                System.out.println("HILO-" + Thread.currentThread().getName() 
                    + " La ejecución no puede ser cancelada");
        
        proceso.setEstado(EN_EJECUCION);
        
        try {
            // Simula el tiempo de ejecución del proceso
            TimeUnit.SECONDS.sleep(proceso.tiempoEjecucion());
                
        } catch (InterruptedException ex) {
            // Si se produce la interrupción mientas está suspendido hay que tratarla
            // por si ya se ha superado el % de cancelación y no es posible su finalización
            if ( completados < (listaProcesos.size() * NO_CANCEL /  TOTAL))
                throw new InterruptedException();
            else 
                System.out.println("HILO-" + Thread.currentThread().getName() 
                        + " La ejecución no puede ser cancelada");
        }
        
        proceso.setEstado(FINALIZADO);
        completados++;
    }
    
    /**
     * Presenta la lista de procesos con su estado de ejecución
     */
    private void presentarEjecucion() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Estado de la lista de procesos: \n\t" + listaProcesos);
    }
    
    /**
     * Cambia el estado de los procesos a CANCELADO y muestra el mensaje
     * indicando el estado de cancelación de la unidad de procesamiento
     */
    private void cancelacionEjecucion() {
        for(int i = completados; i < listaProcesos.size(); i++)
            listaProcesos.get(i).setEstado(CANCELADO);
        System.out.println("Se ha interrumpido la ejecución del HILO-" +
                    Thread.currentThread().getName());
    }
}
