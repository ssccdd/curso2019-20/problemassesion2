/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo1.Constantes.COMPONENTES;
import java.util.Arrays;

/**
 *
 * @author pedroj
 */
public class Ordenador {
    private int iD;
    private final Fabricante[] componentes;

    /**
     * Crea un ordenador sin elementos de vendedor asignados
     */
    public Ordenador() {
        this.componentes = new Fabricante[COMPONENTES.length];
        for( Fabricante vendedor : componentes)
            vendedor = null;
        
        this.iD = 0;
    }

    public int getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public Fabricante[] getComponentes() {
        return componentes;
    }
    
    /**
     * Asigna el nuevo vendedor al ordenador
     * @param vendedor para añadir al ordenador
     * @return true si se a añadido false si ya tenía ese vendedor
     */
    public boolean addComponente( Fabricante vendedor) {
        boolean resultado = false;
        
        if( componentes[vendedor.getComponente().ordinal()] == null ) {
            componentes[vendedor.getComponente().ordinal()] = vendedor;
            resultado = true;
        }
        
        return resultado;
    }

    @Override
    public String toString() {
        String resultado = null;
        
        if ( ordenadorCompleto() )
            resultado = "Ordenador[" + iD + "]{" + 
                            "componentes=" + Arrays.toString(componentes) + '}';
        else
            resultado = "Ordenador[" + iD + "]{NO_COMPLETO}";
        
        return resultado; 
    }
    
    private boolean ordenadorCompleto() {
        boolean completo = true;
        int i = 0;
        
        while( (i < componentes.length) &&  completo ) 
            if ( componentes[i] != null )
                i++;
            else 
                completo = false;
        
        return componentes.length == i;
    }
}
