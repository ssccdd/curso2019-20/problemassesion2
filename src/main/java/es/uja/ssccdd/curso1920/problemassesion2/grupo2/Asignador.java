/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion2.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion2.grupo2.Constantes.PORCENTAJE_CANCELAR;
import static es.uja.ssccdd.curso1920.problemassesion2.grupo4.Constantes.TOTAL_INSTALADORES;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fconde
 */
public class Asignador implements Runnable {
    
    private final String ID;
    private final Ordenador[] ordenadores;
    private final Fotograma[] fotogramas;
    private int fotogramasAsignados;
    // - Contador que permite recorrer el array de ordenadores a partir del
    //   ultimo asignado, de forma que la asignacion de fotogramas a ordenadores
    //   es mas equitativa.
    private int siguienteAComprobar;

    public Asignador(String ID, Ordenador[] ordenadores, Fotograma[] fotogramas) {
        this.ID = ID;
        this.ordenadores = ordenadores;
        this.fotogramas = fotogramas;
        this.fotogramasAsignados = 0;
        this.siguienteAComprobar = 0;
    }

    public String getID() {
        return ID;
    }    
    
    @Override
    public void run() {
        System.out.println("HILO-" + Thread.currentThread().getName() + 
                    " Empieza la asignacion de sus " + fotogramas.length +
                    " fotogramas en sus " + ordenadores.length +
                    " ordenadores");
        
        boolean cancelado = false;
        int j = 0;
        while (!cancelado && j < fotogramas.length) {
            try {
                asignaFotograma(fotogramas[j++]);
                // - Seria un error escribir:
                //   asignaFotograma(fotogramas[j]);
                //   j++;
                //   ya que podria recibirse la peticion de interrumpir el hilo
                //   en asignaFotograma y nunca se actualizaria j con j++
            } catch (InterruptedException ex) {
                float porcentaje = (float)fotogramasAsignados / fotogramas.length;
                System.out.println("Se ha pedido interrumpir la ejecución del HILO-" +
                        Thread.currentThread().getName() + " LLevo :" + 
                        fotogramasAsignados + " de " + fotogramas.length +
                        ", el " + (porcentaje * 100) + "%");
                if (porcentaje > PORCENTAJE_CANCELAR) {
                    System.out.println("Cancelando el HILO-" + Thread.currentThread().getName() + "\n");
                    cancelado = true;
                } else {
                    System.out.println("Ignorando la cancelación del HILO-" +
                            Thread.currentThread().getName() + "\n");
                }
            }
        }
         
        mostrarEstado();
    }
    
    private void asignaFotograma(Fotograma fotograma) throws InterruptedException {
        boolean asignado = false;
        int j = siguienteAComprobar;
        while(!asignado && (j<siguienteAComprobar + ordenadores.length)) {
            asignado = ordenadores[j%ordenadores.length].asignaFotograma(fotograma);
            j++;
        }
        siguienteAComprobar = j%ordenadores.length;        
        if (asignado) { 
            fotogramasAsignados++; 
            try {
                TimeUnit.SECONDS.sleep(fotograma.getTiempoAsignacion());
            } catch (InterruptedException ex) {
                throw ex;
            }
        }
    }
    
    private void mostrarEstado() {
        String cadena = "HILO-" + Thread.currentThread().getName() + "\n";
        cadena += "Fotogramas no asignados: " + (fotogramas.length - fotogramasAsignados) + "\n";
        
        for (Ordenador o: ordenadores) {
            cadena += o;
        }
        
        System.out.println(cadena);
    }
}
